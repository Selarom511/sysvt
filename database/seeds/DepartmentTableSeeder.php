<?php

use App\Department;
use Illuminate\Database\Seeder;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('department')->delete();
		Department::create(array('name' => 'Ahuachapán','country_id' => 1));
        Department::create(array('name' => 'Santa Ana','country_id' => 1));
        Department::create(array('name' => 'Sonsonate','country_id' => 1));
        Department::create(array('name' => 'Usulután','country_id' => 1));
        Department::create(array('name' => 'San Miguel','country_id' => 1));
        Department::create(array('name' => 'Morazán','country_id' => 1));
        Department::create(array('name' => 'La Unión','country_id' => 1));
        Department::create(array('name' => 'La Libertad','country_id' => 1));
        Department::create(array('name' => 'Chalatenango','country_id' => 1));
        Department::create(array('name' => 'Cuscatlán','country_id' => 1));
        Department::create(array('name' => 'San Salvador','country_id' => 1));
        Department::create(array('name' => 'La Paz','country_id' => 1));
        Department::create(array('name' => 'Cabañas','country_id' => 1));
        Department::create(array('name' => 'San Vicente','country_id' => 1));
        Department::create(array('name' => 'Alabama','country_id' => 2));
        Department::create(array('name' => 'Alaska','country_id' => 2));
        Department::create(array('name' => 'Arizona','country_id' => 2));
        Department::create(array('name' => 'Arkansas','country_id' => 2));
        Department::create(array('name' => 'California','country_id' => 2));
        Department::create(array('name' => 'Carolina del Norte','country_id' => 2));
        Department::create(array('name' => 'Carolina del Sur','country_id' => 2));
        Department::create(array('name' => 'Colorado','country_id' => 2));
        Department::create(array('name' => 'Connecticut','country_id' => 2));
        Department::create(array('name' => 'Dakota del Norte','country_id' => 2));
        Department::create(array('name' => 'Dakota del Sur','country_id' => 2));
        Department::create(array('name' => 'Delaware','country_id' => 2));
        Department::create(array('name' => 'Florida','country_id' => 2));
        Department::create(array('name' => 'Georgia','country_id' => 2));
        Department::create(array('name' => 'Hawaii','country_id' => 2));
        Department::create(array('name' => 'Idaho','country_id' => 2));
        Department::create(array('name' => 'Illinois','country_id' => 2));
        Department::create(array('name' => 'Indiana','country_id' => 2));
        Department::create(array('name' => 'Iowa','country_id' => 2));
        Department::create(array('name' => 'Kansas','country_id' => 2));
        Department::create(array('name' => 'Kentucky','country_id' => 2));
        Department::create(array('name' => 'Luisiana','country_id' => 2));
        Department::create(array('name' => 'Maine','country_id' => 2));
        Department::create(array('name' => 'Maryland','country_id' => 2));
        Department::create(array('name' => 'Massachusetts','country_id' => 2));
        Department::create(array('name' => 'Míchigan','country_id' => 2));
        Department::create(array('name' => 'Minnesota','country_id' => 2));
        Department::create(array('name' => 'Misisipi','country_id' => 2));
        Department::create(array('name' => 'Misuri','country_id' => 2));
        Department::create(array('name' => 'Montana','country_id' => 2));
        Department::create(array('name' => 'Nebraska','country_id' => 2));
        Department::create(array('name' => 'Nevada','country_id' => 2));
        Department::create(array('name' => 'Nueva Jersey','country_id' => 2));
        Department::create(array('name' => 'Nueva York','country_id' => 2));
        Department::create(array('name' => 'Nuevo Hampshire','country_id' => 2));
        Department::create(array('name' => 'Nuevo México','country_id' => 2));
        Department::create(array('name' => 'Ohio','country_id' => 2));
        Department::create(array('name' => 'Oklahoma','country_id' => 2));
        Department::create(array('name' => 'Oregón','country_id' => 2));
        Department::create(array('name' => 'Pensilvania','country_id' => 2));
        Department::create(array('name' => 'Rhode Island','country_id' => 2));
        Department::create(array('name' => 'Tennessee','country_id' => 2));
        Department::create(array('name' => 'Texas','country_id' => 2));
        Department::create(array('name' => 'Utah','country_id' => 2));
        Department::create(array('name' => 'Vermont','country_id' => 2));
        Department::create(array('name' => 'Virginia','country_id' => 2));
        Department::create(array('name' => 'Virginia Occidental','country_id' => 2));
        Department::create(array('name' => 'Washington','country_id' => 2));
        Department::create(array('name' => 'Wisconsin','country_id' => 2));
        Department::create(array('name' => 'Wyoming','country_id' => 2));
    }
}
