<?php

use App\Committee;
use Illuminate\Database\Seeder;

class CommitteeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('committee')->delete();
		Committee::create(array('name' => 'Comité Ejecutivo','department_id' => 11));
        Committee::create(array('name' => 'Consejo de Directores','department_id' => 11));
    }
}
