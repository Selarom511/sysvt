<?php

use App\Committee;
use App\User;
use Illuminate\Database\Seeder;

class CandidateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('candidate')->delete();
		User::find(2)->committee()->attach([1,2]);
		User::find(3)->committee()->attach([1]);
    }
}
