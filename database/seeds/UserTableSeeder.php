<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->delete();
		User::create(array('id_document' => '00000000-0','name' => 'Admin', 'surname' => 'Admin', 'email' => 'selarom511@gmail.com','password' => Hash::make('123456'),'admin' => true,'department_id' => 11));
        User::create(array('id_document' => '00000000-1','name' => 'Usuario', 'surname' => '1', 'email' => 'usuario1@gmail.com','password' => Hash::make('123456'),'admin' => false,'department_id' => 11));
        User::create(array('id_document' => '00000000-2','name' => 'Usuario', 'surname' => '2', 'email' => 'usuario2@gmail.com','password' => Hash::make('123456'),'admin' => false,'department_id' => 2));
    }
}
