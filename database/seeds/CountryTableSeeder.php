<?php

use App\Country;
use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('country')->delete();
		Country::create(array('name' => 'El Salvador'));
		Country::create(array('name' => 'Estados Unidos'));
    }
}
