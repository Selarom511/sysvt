<div class="modal fade bs-example-modal-lg" id="mdlUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="mdlUser_lblModal">Postulación de candidatura a comité</h4>
			</div>
			{!! Form::open(['class' => 'form-horizontal', 'role' => 'form', 'name' => 'frmUser', 'id' => 'frmUser']) !!}
				<div class="modal-body">
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-10 col-sm-offset-1">
								<br>
								<div class="alert alert-info fade in" >
									<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
									<strong>Importante</strong>
									<br/><br/>
									<b>1) </b>Seleccione el o los comités a los que se desea postular como candidato.<br>
								</div>
								<fieldset>
									<div class="form-group">
										<div class="col-sm-6">
											{!! Form::text('frmUser_hddId',null,['class' => 'form-control', 'id' => 'frmUser_hddId']) !!}
											<strong style="color:red; font-weight: bold;" >*</strong>
											{!! Form::label('frmUser_lblCountry', 'País') !!}
											{!! Form::select('frmUser_slcCountry',$countryArray,0,['class' => 'form-control', 'id' => 'frmUser_slcCountry']) !!}
										</div>
										<div class="col-sm-6">
											<strong style="color:red; font-weight: bold;" >*</strong>
											{!! Form::label('frmUser_lblDepartment', 'Departamento') !!}
											{!! Form::select('frmUser_slcDepartment',[0 => 'Seleccione un departamento'],0,['class' => 'form-control', 'id' => 'frmUser_slcDepartment']) !!}
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-12">
											{!! Form::label('frmUser_lblCommittee', 'Comité a asignar') !!}
											{!! Form::select('frmUser_slcCommittee[]',[],null,['class' => 'form-control', 'id' => 'frmUser_slcCommittee', 'style' => 'width:100%;', 'multiple']) !!}
										</div>
									</div>
									<div class="col sm-12">&nbsp;</div>
									<div class="form-group">
										<div class="col-sm-12">
											{!! Form::label('frmUser_lblCommittee', 'Comités actuales') !!}
											{!! Form::select('frmUser_slcActCommittee[]',$committeeArray,null,['class' => 'form-control', 'id' => 'frmUser_slcActCommittee', 'style' => 'width:100%;', 'multiple']) !!}
										</div>
									</div>
								</fieldset>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
                	{!! Form::button('Asignar',['class' => 'btn btn-primary', 'id' => 'frmUser_btnEnviar']) !!}
					{!! Form::button('Cancelar',['class' => 'btn btn-default', 'id' => 'frmUser_btnCancelar', 'data-dismiss' => 'modal']) !!}
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
