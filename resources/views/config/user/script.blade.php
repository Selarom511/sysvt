<script type="text/javascript">
// Inicializacion de estilo de la tabla
    tblUser = $('#tblUser').DataTable({
        "order": [
            [ 1, "asc" ]
        ],
        "aLengthMenu": [
            [10, 25, 50, -1], 
            [10, 25, 50, "All"]
        ],
        "columnDefs": [
            { className: "text-center", "targets": [0,3,4,5,6,7,8]},
        ],
        "language": {
            "lengthMenu"    : "Mostrar _MENU_ usuarios",
            "zeroRecords"   : "No hay coincidencias",
            "info"          : "Mostrando p&aacute;gina _PAGE_ de _PAGES_",
            "infoEmpty"     : "No hay usuarios disponibles",
            "infoFiltered"  : "(filtrado de un total de _MAX_ registros)",
            "sSearch"       : "Buscar:",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "&Uacute;ltimo",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                        },
        },
        "columns": [
            { "width": "9%" },
            { "width": "19%" },
            { "width": "19%" },
            { "width": "14%" },
            { "width": "14%" },
            { "width": "10%" },
            { "width": "5%" },
            { "width": "5%" },
            { "width": "5%" }
        ]
    })

// Asignación de estado de controles
    $('#frmSearchUser_slcDepartment').attr('disabled',true)
    $('#frmUser_hddId').hide()
    $('#frmUser_slcCommittee').select2()
    $('#frmUser_slcActCommittee').select2()

// Función de búsqueda de departamentos en relación al país seleccionado
    function getDepartments(country, depto, selected) {
        if (country.val() == 0) {
            depto.html('<option value="0" selected>Seleccione un departamento</option>')
            depto.attr('disabled',true)
        } else {
            var url = "{{ URL::route('getDepartments', ['id' => ':id']) }}"
            url = url.replace(':id', (country.val()))
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    if (data.success == true) {
                        depto.html('')
                        for (var i = 0; i < data.departmentArray.length; i++) {
                            var department = data.departmentArray[i]
                            if (department.id == selected)
                                depto.append("<option value='"+department.id+"' selected>"+department.name+"</option>")
                            else
                                depto.append("<option value='"+department.id+"'>"+department.name+"</option>")
                        }
                        depto.attr('disabled',false)
                    } else {
                        depto.attr('disabled',true)
                        alertify.error(data.message)
                    }
                },
                error: function(errors){
                    alertify.error("<span style='color:white;'>Error de comunicacion con el servidor, por favor contacte al encargado de soporte.</span>")
                }
            })
        }
    }

// Función de búsqueda de comités en relación al departamento seleccionado
    function getCommittees(depto, comit, selected) {
        $('#frmUser_slcCommittee').val(null).trigger("change")
        var url = "{{ URL::route('getCommittees', ['depto' => ':depto']) }}"
        url = url.replace(':depto', (depto.val()))
        $.ajax({
            type: 'GET',
            url: url,
            success: function (data) {
                if (data.success == true) {
                    comit.html('')
                    for (var i = 0; i < data.committeeArray.length; i++) {
                        var committee = data.committeeArray[i]
                        comit.append("<option value='"+committee.id+"'>"+committee.name+"</option>")
                    }
                    comit.attr('disabled',false)
                } else {
                    comit.attr('disabled',true)
                    alertify.error(data.message)
                }
                alertify.alert().close();
                $('#mdlUser').modal('show')
            },
            error: function(errors){
                alertify.error("<span style='color:white;'>Error de comunicacion con el servidor, por favor contacte al encargado de soporte.</span>")
            }
        })
    }

// Evento de cambio de país para búsqueda
    $('#frmSearchUser_slcCountry').change(function () {
        getDepartments($(this), $('#frmSearchUser_slcDepartment'), 0)
    })

// Evento de cambio de país para formulario
    $('#frmUser_slcCountry').change(function () {
        getDepartments($(this), $('#frmUser_slcDepartment'), 0)
    })

// Evento de cambio de departamento para formulario
    $('#frmUser_slcDepartment').change(function () {
        getCommittees($(this), $('#frmUser_slcCommittee'), '')
    })

// Función de búsqueda de usuarios
    $('#frmSearchUser_btnBuscar').click(function () {
        var $btn = $(this).button('loading')
        var url = "{{ URL::route('searchUser') }}"
        var form = $('#frmSearchUser').serialize()
        $.ajax({
            type: 'POST',
            url: url,
            data: form,
            success: function (data) {
                if (data.success == true) {
                    tblUser.clear().draw()
                    for (var i = 0; i < data.users.length; i++) {
                        var user = data.users[i]
                        var estadoC = 'default'
                        var estadoB = 'unchecked'
                        if (user.admin_nom == 'Si') {
                            estadoC = 'success'
                            estadoB = 'check'
                        }
                        tblUser.row.add([
                            '<span id="tblUser_document'+user.id+'" >'+user.id_document+'</span>',
                            '<span id="tblUser_name'+user.id+'" >'+user.name+'</span>',
                            '<span id="tblUser_surname'+user.id+'" >'+user.surname+'</span>',
                            '<span id="tblUser_departmentNom'+user.id+'" >'+user.department_nom+'</span>',
                            '<span id="tblUser_countryNom'+user.id+'" >'+user.country_nom+'</span>',
                            '<span id="tblUser_adminNom'+user.id+'" >'+user.admin_nom+'</span>',
                            '<button class="btn btn-primary" onClick="mdlCommittee('+user.id+')" ><span class="glyphicon glyphicon-pencil"></span></button>',
                            '<button class="btn btn-'+estadoC+'" id="tblUser_btnAdmin'+user.id+'" onClick="administrator('+user.id+')" ><span class="glyphicon glyphicon-'+estadoB+'"></span></button>',
                            '<button class="btn btn-danger" id="tblUser_btnEliminar'+user.id+'" onClick="deleteU('+user.id+')" ><span class="glyphicon glyphicon-trash"></span></button>'+
                            '<input type="hidden" name="tblUser_hddAdminId'+user.id+'" id="tblUser_hddAdminId'+user.id+'" value="'+user.admin+'" >'+
                            '<input type="hidden" name="tblUser_hddCountryId'+user.id+'" id="tblUser_hddCountryId'+user.id+'" value="'+user.country_id+'" >'+
                            '<input type="hidden" name="tblUser_hddDepartmentId'+user.id+'" id="tblUser_hddDepartmentId'+user.id+'" value="'+user.department_id+'" >'+
                            '<input type="hidden" name="tblUser_hddCommittee'+user.id+'" id="tblUser_hddCommittee'+user.id+'" value="'+user.committees+'" >',
                        ]).draw()
                    }
                } else {
                    alertify.error(data.message)
                }
                $btn.button('reset')
            },
            error: function(errors){
                alertify.error("<span style='color:white;'>Error de comunicacion con el servidor, por favor contacte al encargado de soporte.</span>")
            }
        })
    })

// Función que limpia el formulario Usuario
    function clearForm(modal) {
        modal.find('.modal-body #frmUser_slcCountry').prop('selectedIndex', 0)
        getDepartments($('#frmUser_slcCountry'), $('#frmUser_slcDepartment'), 0) 
        modal.find('.modal-body #frmUser_slcCommittee').prop('selectedIndex', -1).html('').trigger('change')

        modal.find('.modal-body #frmUser_hddId').hide()
    }

// Función que muestra la modal de usuario
    function mdlCommittee(recipient) {
        alertify.alert("<center><h4>Cargando datos de "+$('#tblUser_name'+recipient).html()+" "+$('#tblUser_surname'+recipient).html()+"...</h4><br><img id=\"cargador\" src=\"{!! URL::asset('images/ajax-loader.gif') !!}\"/></center>")
                .set('basic',true)
                .set('movable', false)
                .set('closable', false)
        var modal = $('#mdlUser')
        modal.find('.modal-body #frmUser_hddId').val(recipient)
        modal.find('.modal-body #frmUser_slcCountry').val($('#tblUser_hddCountryId'+recipient).val())
        getDepartments($('#frmUser_slcCountry'), $('#frmUser_slcDepartment'), $('#tblUser_hddDepartmentId'+recipient).val())
        getCommittees($('#frmUser_slcDepartment'), $('#frmUser_slcCommittee'), $('#tblUser_hddCommittee'+recipient).val())
        modal.find('.modal-body #frmUser_slcCommittee').prop('selectedIndex', -1).trigger('change')
        $.each($('#tblUser_hddCommittee'+recipient).val().split(","), function(i,e){
            modal.find('.modal-body #frmUser_slcActCommittee').find('option').filter(function() {
                return $(this).val() == e
            }).prop('selected', true).trigger("change")
        });
    }

// Evento cuando se cierra la modal de usuario
    $('#mdlUser').on('hidden.bs.modal', function (event) {
        clearForm($(this))
    })

// Función de creación o modificación de un usuario
    $('#frmUser_btnEnviar').click(function () {
        alertify.alert("<center><h4>Por favor espere, su información esta siendo procesada.</h4><br><img id=\"cargador\" src=\"{!! URL::asset('images/ajax-loader.gif') !!}\"/></center>")
                .set('basic',true)
                .set('movable', false)
                .set('closable', false)
        var accion = $(this).val()
        var form = $('#frmUser').serialize()
        var url = "{{ URL::route('committeeUser') }}"
        $.ajax({
            type: 'POST',
            url: url,
            data: form,
            success: function (data) {
                if (data.success == true) {
                    user = data.user
                    $('#tblUser_hddCommittee'+user.id).val(user.jefes)

                    clearForm($('#mdlUser'))
                    alertify.success(data.message)
                } else {
                    alertify.error(data.message)
                }
                alertify.alert().close();
                $('#mdlUser').modal('hide')
            },
            error: function(errors){
                alertify.error("<span style='color:white;'>Error de comunicación con el servidor</span>")
            }
        })
    })

// Función para cambiar el estado de un usuario
    function administrator(id) {
        var estado = 'activar'
        if ($('#tblUser_adminNom'+id).html() == 'Si')
            estado = 'inactivar'
        alertify.confirm("<center><span style='color:black; font-weight: bold; font-size: 16px;'>¿Desea "+estado+" este registro como administrador?</span></center>").set('onok', function(closeEvent){
            var url = "{{ URL::route('adminUser', ['id' => ':id']) }}"
            url = url.replace(':id', id)
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    if (data.success == true) {
                        if (estado == 'activar') {
                            $('#tblUser_btnAdmin'+id).html('<span class="glyphicon glyphicon-check"></span>')
                            $('#tblUser_btnAdmin'+id).removeClass( "btn-default" ).addClass( "btn-success" )
                            $('#tblUser_adminNom'+id).html('Si')
                            alertify.success('<span style="color:white;">Se han dado permisos de administrador</span>')
                        } else {
                            $('#tblUser_btnAdmin'+id).html('<span class="glyphicon glyphicon-unchecked"></span>')
                            $('#tblUser_btnAdmin'+id).removeClass( "btn-success" ).addClass( "btn-default" )
                            $('#tblUser_adminNom'+id).html('No')
                            alertify.error('<span style="color:white;">Se han revocado permisos de administrador</span>')
                        }
                    } else {
                        alertify.error(data.message)
                    }
                },
                error: function(errors){
                    alertify.error("<span style='color:white;'>Error de comunicacion con el servidor, por favor contacte al encargado de soporte.</span>")
                }
            })
        }).set('modal', false).setHeader('Alerta')
    }

// Función para eliminar un usuario
    function deleteU(id) {
        alertify.confirm("<center><span style='color:black; font-weight: bold; font-size: 16px;'>¿Desea eliminar este registro?</span></center>").set('onok', function(closeEvent){
            var url = "{{ URL::route('deleteUser', ['id' => ':id']) }}"
            url = url.replace(':id', id)
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    if (data.success == true) {
                        tblUser.row($('#tblUser_document'+id).parents('tr')).remove().draw()
                        alertify.success(data.message)
                    } else {
                        alertify.error(data.message)
                    }
                },
                error: function(errors){
                    alertify.error("<span style='color:white;'>Error de comunicacion con el servidor, por favor contacte al encargado de soporte.</span>")
                }
            })
        }).set('modal', false).setHeader('Alerta')
    }
</script>
