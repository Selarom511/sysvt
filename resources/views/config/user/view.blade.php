@extends('layouts.app')

@section('subtitulo')
    Administración de Usuario
@stop

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="text-center">
                        <h1>Administrar Usuarios</h1>
                    </div>
                </div>
                <div class="col-md-10 col-md-offset-1">
                    @include('config.user.formbuscar')
                </div>
                <div class="col-md-12">
                    <br>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            @include('config.user.tabla')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('config.user.formulario')
    @include('config.user.script')
@stop
