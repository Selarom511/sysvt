{!! Form::open(['class' => 'form-horizontal', 'role' => 'form', 'name' => 'frmSearchUser', 'id' => 'frmSearchUser']) !!}
    <div class="panel panel-primary">
        <div class="panel-heading"><b>Busqueda de usuarios</b></div>
        <div class="panel-body">
            <div class="form-group">
                <div class="col-lg-4">
                    {!! Form::label('frmSearchUser_lblName', 'Nombre') !!}
                    {!! Form::text('frmSearchUser_txtName',null,['class' => 'form-control', 'placeholder' => 'Nombre', 'id' => 'frmSearchUser_txtName']) !!}
                </div>
                <div class="col-lg-4">
                    {!! Form::label('frmSearchUser_lblSurname', 'Apellido') !!}
                    {!! Form::text('frmSearchUser_txtSurname',null,['class' => 'form-control', 'placeholder' => 'Apellido', 'id' => 'frmSearchUser_txtSurname']) !!}
                </div>
                <div class="col-lg-4">
                    {!! Form::label('frmSearchUser_lblDocument_id', 'Documento') !!}
                    {!! Form::text('frmSearchUser_txtDocument_id',null,['class' => 'form-control', 'placeholder' => 'Correo', 'id' => 'frmSearchUser_txtDocument_id']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-4">
                    {!! Form::label('frmSearchUser_lblEmail', 'Email') !!}
                    {!! Form::text('frmSearchUser_txtEmail',null,['class' => 'form-control', 'placeholder' => 'Correo', 'id' => 'frmSearchUser_txtEmail']) !!}
                </div>
                <div class="col-lg-4">
                    {!! Form::label('frmSearchUser_lblCountry', 'País') !!}
                    {!! Form::select('frmSearchUser_slcCountry',$countryArray,0,['class' => 'form-control', 'id' => 'frmSearchUser_slcCountry']) !!}
                </div>
                <div class="col-lg-4">
                    {!! Form::label('frmSearchUser_lblDepartment', 'Departamento') !!}
                    {!! Form::select('frmSearchUser_slcDepartment',$departmentArray,0,['class' => 'form-control', 'id' => 'frmSearchUser_slcDepartment']) !!}
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
            {!! Form::button('Buscar',['class' => 'btn btn-sm btn-primary', 'id' => 'frmSearchUser_btnBuscar', 'data-loading-text' => 'Cargando registros...', 'autocomplete' => 'off']) !!}
        </div>
    </div>
{!! Form::close() !!}