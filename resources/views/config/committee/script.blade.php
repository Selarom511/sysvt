<script type="text/javascript">
// Inicializacion de estilo de la tabla
    tblCommittee = $('#tblCommittee').DataTable({
        "aLengthMenu": [
            [5, 10, 25, -1], 
            [5, 10, 25, "All"]
        ],
        "columnDefs": [
            { className: "text-center", "targets": [0,1,2,3] },
        ],
        "language": {
            "lengthMenu"    : "Mostrar _MENU_ áreas",
            "zeroRecords"   : "No hay coincidencias",
            "info"          : "Mostrando p&aacute;gina _PAGE_ de _PAGES_",
            "infoEmpty"     : "No hay áreas disponibles",
            "infoFiltered"  : "(filtrado de un total de _MAX_ registros)",
            "sSearch"       : "Buscar:",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "&Uacute;ltimo",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                        },
        },
        "columns": [
            { "width": "5%" },
            { "width": "45%" },
            { "width": "15%" },
            { "width": "15%" }
        ]
    })

// Asignación de estado de controles
    $('#slcDepartment').attr('disabled',true)
    $('#frmCommittee_hddId').hide()
    $('#frmCommittee_hddDepartment').hide()
    $('#frmCommittee_btnEnviar').val('Crear')

// Función de búsqueda de departamentos en relación al país seleccionado
    $('#slcCountry').change(function() {
        if ($('#slcCountry').val() == 0) {
            $('#slcDepartment').html('<option value="0" selected>Seleccione un departamento</option>')
            $('#slcDepartment').attr('disabled',true)
        } else {
            var url = "{{ URL::route('getDepartments', ['id' => ':id']) }}"
            url = url.replace(':id', ($('#slcCountry').val()))
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    if (data.success == true) {
                        tblCommittee.clear().draw()
                        $('#slcDepartment').html('')
                        for (var i = 0; i < data.departmentArray.length; i++) {
                            var department = data.departmentArray[i]
                            $('#slcDepartment').append("<option value='"+department.id+"'>"+department.name+"</option>")
                        }
                        $('#slcDepartment').attr('disabled',false)
                    } else {
                        $('#slcDepartment').attr('disabled',true)
                        alertify.error(data.message)
                    }
                },
                error: function(errors){
                    alertify.error("<span style='color:white;'>Error de comunicacion con el servidor, por favor contacte al encargado de soporte.</span>")
                }
            })
        }
    })

// Función de búsqueda de áreas en relación al departamento seleccionado
    $('#slcDepartment').change(function () {
        var url = "{{ URL::route('searchCommittee', ['id' => ':id']) }}"
        url = url.replace(':id', ($('#slcDepartment').val()))
        $.ajax({
            type: 'GET',
            url: url,
            success: function (data) {
                if (data.success == true) {
                    $('#frmCommittee_hddDepartment').val($('#slcDepartment').val())
                    tblCommittee.clear().draw()
                    for (var i = 0; i < data.committees.length; i++) {
                        var committee = data.committees[i]
                        tblCommittee.row.add([
                            '<span id="tblCommittee_id'+committee.id+'">'+committee.id+'</span>',
                            '<span id="tblCommittee_name'+committee.id+'" >'+committee.name+'</span>',
                            '<button class="btn btn-primary" onClick="editar('+committee.id+')" ><span class="glyphicon glyphicon-pencil"></span></button>',
                            '<button class="btn btn-danger" onClick="eliminar('+committee.id+')" ><span class="glyphicon glyphicon-trash"></span></button>',
                        ]).draw()
                    }
                } else {
                    alertify.error(data.message)
                }
            },
            error: function(errors){
                alertify.error("<span style='color:white;'>Error de comunicacion con el servidor, por favor contacte al encargado de soporte.</span>")
            }
        })
    })

// Función de creación o modificación de un comité
    $('#frmCommittee_btnEnviar').click(function () {
        var accion = $(this).val()
        var form = $('#frmCommittee').serialize()
        if (accion == 'Crear')
            var url = "{{ URL::route('createCommittee') }}"
        else
            var url = "{{ URL::route('updateCommittee') }}"
        $.ajax({
            type: 'POST',
            url: url,
            data: form,
            success: function (data) {
                if (data.success == true) {
                    committee = data.committee
                    if (accion == 'Crear') {
                        tblCommittee.row.add([
                            '<span id="tblCommittee_id'+committee.id+'">'+committee.id+'</span>',
                            '<span id="tblCommittee_name'+committee.id+'" >'+committee.name+'</span>',
                            '<button class="btn btn-primary" onClick="editar('+committee.id+')" ><span class="glyphicon glyphicon-pencil"></span></button>',
                            '<button class="btn btn-danger" onClick="eliminar('+committee.id+')" ><span class="glyphicon glyphicon-trash"></span></button>',
                        ]).draw()
                    }else{
                        $('#tblCommittee_name'+committee.id).html(committee.name)
                    }
                    $('#frmCommittee_leyenda').html('Nuevo Comité')
                    $('#frmCommittee_hddId').val('')
                    $('#frmCommittee_txtName').val('')
                    $('#frmCommittee_btnEnviar').val('Crear')
                    $('#frmCommittee_btnEnviar').html('Crear')
                    alertify.success(data.message)
                } else {
                    alertify.error(data.message)
                }
            },
            error: function(errors){
                alertify.error("<span style='color:white;'>Error de comunicación con el servidor</span>")
            }
        })
    })

// función de limpieza del formulario
    $('#frmCommittee_btnCancelar').click(function() {
        $('#frmCommittee_leyenda').html('Nuevo Comité')
        $('#frmCommittee_hddId').val('')
        $('#frmCommittee_txtName').val('')
        $('#frmCommittee_btnEnviar').val('Crear')
        $('#frmCommittee_btnEnviar').html('Crear')
    })

// Función que transfiere los datos de la fila a modificarse, hacia el formulario
    function editar(id) {
        $('#frmCommittee_leyenda').html('Modificar Comité')
        $('#frmCommittee_hddId').val(id)
        $('#frmCommittee_txtName').val($('#tblCommittee_name'+id).html())
        $('#frmCommittee_btnEnviar').val('Guardar Cambios')
        $('#frmCommittee_btnEnviar').html('Guardar Cambios')
    }

// Función para eliminar un comité
    function eliminar(id) {
        alertify.confirm("<center><span style='color:black; font-weight: bold; font-size: 16px;'>¿Desea eliminar este registro?</span></center>").set('onok', function(closeEvent){
            var url = "{{ URL::route('deleteCommittee', ['id' => ':id']) }}"
            url = url.replace(':id', id)
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    if (data.success == true) {
                        tblCommittee.row($('#tblCommittee_id'+id).parents('tr')).remove().draw()
                        alertify.success(data.message)
                    } else {
                        alertify.error(data.message)
                    }
                },
                error: function(errors){
                    alertify.error("<span style='color:white;'>Error de comunicacion con el servidor, por favor contacte al encargado de soporte.</span>")
                }
            })
        }).set('modal', false).setHeader('Alerta')
    }
</script>