<div class="panel-heading"><span id="frmCommittee_leyenda" style="font-weight: bold;" >Nuevo Comité</span></div>
{!! Form::open(['class' => 'form-horizontal', 'role' => 'form', 'name' => 'frmCommittee', 'id' => 'frmCommittee']) !!}
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<br>
				<fieldset>
					<div class="form-group">
						<div class="col-sm-12">
							{!! Form::text('frmCommittee_hddId',null,['class' => 'form-control', 'id' => 'frmCommittee_hddId']) !!}
							{!! Form::text('frmCommittee_hddDepartment',null,['class' => 'form-control', 'id' => 'frmCommittee_hddDepartment']) !!}
							<b>Nombre: </b>
							{!! Form::text('frmCommittee_txtName',null,['class' => 'form-control', 'id' => 'frmCommittee_txtName']) !!}
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<div class="panel-footer text-right">
		{!! Form::button('Crear',['class' => 'btn btn-primary', 'id' => 'frmCommittee_btnEnviar', 'value' => 'Crear']) !!}
		{!! Form::button('Cancelar',['class' => 'btn btn-default', 'id' => 'frmCommittee_btnCancelar']) !!}
	</div>
{!! Form::close() !!}