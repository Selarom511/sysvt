<script type="text/javascript">
// Inicializacion de estilo de la tabla
    tblCountry = $('#tblCountry').DataTable({
        "aLengthMenu": [
            [5, 10, 25, -1], 
            [5, 10, 25, "All"]
        ],
        "columnDefs": [
            { className: "text-center", "targets": [0,1,2,3]},
        ],
        "language": {
            "lengthMenu"    : "Mostrar _MENU_ paises",
            "zeroRecords"   : "No hay coincidencias",
            "info"          : "Mostrando p&aacute;gina _PAGE_ de _PAGES_",
            "infoEmpty"     : "No hay paises disponibles",
            "infoFiltered"  : "(filtrado de un total de _MAX_ registros)",
            "sSearch"       : "Buscar:",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "&Uacute;ltimo",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                        },
        },
        "columns": [
            { "width": "5%" },
            { "width": "45%" },
            { "width": "15%" },
            { "width": "15%" }
        ]
    })

// Asignación de estado de controles
    $('#frmCountry_hddId').hide()
    $('#frmCountry_btnEnviar').val('Crear')

// Función de creación o modificación de un país
    $('#frmCountry_btnEnviar').click(function () {
        var accion = $(this).val()
        var form = $('#frmCountry').serialize()
        if (accion == 'Crear')
            var url = "{{ URL::route('createCountry') }}"
        else
            var url = "{{ URL::route('updateCountry') }}"
        $.ajax({
            type: 'POST',
            url: url,
            data: form,
            success: function (data) {
                if (data.success == true) {
                    country = data.country
                    if (accion == 'Crear') {
                        tblCountry.row.add([
                            '<span id="tblCountry_id'+country.id+'">'+country.id+'</span>',
                            '<span id="tblCountry_name'+country.id+'" >'+country.name+'</span>',
                            '<button class="btn btn-primary" onClick="editar('+country.id+')" ><span class="glyphicon glyphicon-pencil"></span></button>',
                            '<button class="btn btn-danger" onClick="eliminar('+country.id+')" ><span class="glyphicon glyphicon-trash"></span></button>',
                        ]).draw()
                    }else{
                        $('#tblCountry_name'+country.id).html(country.name)
                    }
                    $('#frmCountry_leyenda').html('Nuevo País')
                    $('#frmCountry_hddId').val('')
                    $('#frmCountry_txtName').val('')
                    $('#frmCountry_btnEnviar').val('Crear')
                    $('#frmCountry_btnEnviar').html('Crear')
                    alertify.success(data.message)
                } else {
                    alertify.error(data.message)
                }
            },
            error: function(errors){
                alertify.error("<span style='color:white;'>Error de comunicación con el servidor</span>")
            }
        })
    })

// función de limpieza del formulario
    $('#frmCountry_btnCancelar').click(function() {
        $('#frmCountry_leyenda').html('Nuevo País')
        $('#frmCountry_hddId').val('')
        $('#frmCountry_txtName').val('')
        $('#frmCountry_btnEnviar').val('Crear')
        $('#frmCountry_btnEnviar').html('Crear')
    })

// Función que transfiere los datos de la fila a modificarse, hacia el formulario
    function editar(id) {
        $('#frmCountry_leyenda').html('Modificar País')
        $('#frmCountry_hddId').val(id)
        $('#frmCountry_txtName').val($('#tblCountry_name'+id).html())
        $('#frmCountry_btnEnviar').val('Guardar Cambios')
        $('#frmCountry_btnEnviar').html('Guardar Cambios')
    }

// Función para eliminar un país
    function eliminar(id) {
        alertify.confirm("<center><span style='color:black; font-weight: bold; font-size: 16px;'>¿Desea eliminar este registro?</span></center>").set('onok', function(closeEvent){
            var url = "{{ URL::route('deleteCountry', ['id' => ':id']) }}"
            url = url.replace(':id', id)
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    if (data.success == true) {
                        tblCountry.row($('#tblCountry_id'+id).parents('tr')).remove().draw()
                        alertify.success(data.message)
                    } else {
                        alertify.error(data.message)
                    }
                },
                error: function(errors){
                    alertify.error("<span style='color:white;'>Error de comunicacion con el servidor, por favor contacte al encargado de soporte.</span>")
                }
            })
        }).set('modal', false).setHeader('Alerta')
    }
</script>