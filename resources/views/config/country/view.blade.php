@extends('layouts.app')

@section('subtitulo')
    Administración de Paises
@stop

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="text-center">
                        <h1>Administrar Paises</h1>
                    </div>
                </div>
                <div class="col-md-7">
                    <br>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            @include('config.country.tabla')
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <br>
                    <div class="panel panel-primary">
                        @include('config.country.formulario')
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('config.country.script')
@stop
