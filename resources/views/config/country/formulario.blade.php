<div class="panel-heading"><span id="frmCountry_leyenda" style="font-weight: bold;" >Nuevo País</span></div>
{!! Form::open(['class' => 'form-horizontal', 'role' => 'form', 'name' => 'frmCountry', 'id' => 'frmCountry']) !!}
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<br>
				<fieldset>
					<div class="form-group">
						<div class="col-sm-12">
							{!! Form::text('frmCountry_hddId',null,['class' => 'form-control', 'id' => 'frmCountry_hddId']) !!}
							<b>Nombre: </b>
							{!! Form::text('frmCountry_txtName',null,['class' => 'form-control', 'id' => 'frmCountry_txtName']) !!}
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<div class="panel-footer text-right">
		{!! Form::button('Crear',['class' => 'btn btn-primary', 'id' => 'frmCountry_btnEnviar', 'value' => 'Crear']) !!}
		{!! Form::button('Cancelar',['class' => 'btn btn-default', 'id' => 'frmCountry_btnCancelar']) !!}
	</div>
{!! Form::close() !!}