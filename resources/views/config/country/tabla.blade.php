<br><br>
<div class="table-responsive">
	<table class="table table-striped display" id="tblCountry">
		<thead>
			<tr>
				<th>#</th>
				<th>País</th>
				<th></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($countries as $country)
				<tr>
					<td><span id="tblCountry_id{{ $country->id }}" >{{ $country->id }}</span></td>
					<td><span id="tblCountry_name{{ $country->id }}" >{{ $country->name }}</span></td>
					<td><button class="btn btn-primary" onClick="editar({{ $country->id }})" ><span class="glyphicon glyphicon-pencil"></span></button></td>
					<td><button class="btn btn-danger" onClick="eliminar({{ $country->id }})" ><span class="glyphicon glyphicon-trash"></span></button></td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>