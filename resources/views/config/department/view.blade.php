@extends('layouts.app')

@section('subtitulo')
    Administración de Departamento
@stop

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="text-center">
                        <h1>Administrar Departamentos</h1>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-4">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">{!! Form::label('lblCountry', 'País') !!}</div>
                            {!! Form::select('slcCountry',$countryArray,0,['class' => 'form-control', 'id' => 'slcCountry']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <br>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            @include('config.department.tabla')
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <br>
                    <div class="panel panel-primary">
                        @include('config.department.formulario')
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('config.department.script')
@stop
