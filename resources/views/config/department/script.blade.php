<script type="text/javascript">
// Inicializacion de estilo de la tabla
    tblDepartment = $('#tblDepartment').DataTable({
        "aLengthMenu": [
            [5, 10, 25, -1], 
            [5, 10, 25, "All"]
        ],
        "columnDefs": [
            { className: "text-center", "targets": [0,1,2,3] },
        ],
        "language": {
            "lengthMenu"    : "Mostrar _MENU_ departamentos",
            "zeroRecords"   : "No hay coincidencias",
            "info"          : "Mostrando p&aacute;gina _PAGE_ de _PAGES_",
            "infoEmpty"     : "No hay departamentos disponibles",
            "infoFiltered"  : "(filtrado de un total de _MAX_ registros)",
            "sSearch"       : "Buscar:",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "&Uacute;ltimo",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                        },
        },
        "columns": [
            { "width": "5%" },
            { "width": "45%" },
            { "width": "15%" },
            { "width": "15%" }
        ]
    })

// Asignación de estado de controles
    $('#frmDepartment_hddId').hide()
    $('#frmDepartment_hddCountry').hide()
    $('#frmDepartment_btnEnviar').val('Crear')

// Función de búsqueda de departamentos en relación al país seleccionado
    $('#slcCountry').change(function () {
        var url = "{{ URL::route('searchDepartment', ['id' => ':id']) }}"
        url = url.replace(':id', ($('#slcCountry').val()))
        $.ajax({
            type: 'GET',
            url: url,
            success: function (data) {
                if (data.success == true) {
                    $('#frmDepartment_hddCountry').val($('#slcCountry').val())
                    tblDepartment.clear().draw()
                    for (var i = 0; i < data.departments.length; i++) {
                        var department = data.departments[i]
                        tblDepartment.row.add([
                            '<span id="tblDepartment_id'+department.id+'">'+department.id+'</span>',
                            '<span id="tblDepartment_name'+department.id+'" >'+department.name+'</span>',
                            '<button class="btn btn-primary" onClick="editar('+department.id+')" ><span class="glyphicon glyphicon-pencil"></span></button>',
                            '<button class="btn btn-danger" onClick="eliminar('+department.id+')" ><span class="glyphicon glyphicon-trash"></span></button>',
                        ]).draw()
                    }
                } else {
                    alertify.error(data.message)
                }
            },
            error: function(errors){
                alertify.error("<span style='color:white;'>Error de comunicacion con el servidor, por favor contacte al encargado de soporte.</span>")
            }
        })
    })

// Función de creación o modificación de un departamento
    $('#frmDepartment_btnEnviar').click(function () {
        var accion = $(this).val()
        var form = $('#frmDepartment').serialize()
        if (accion == 'Crear')
            var url = "{{ URL::route('createDepartment') }}"
        else
            var url = "{{ URL::route('updateDepartment') }}"
        $.ajax({
            type: 'POST',
            url: url,
            data: form,
            success: function (data) {
                if (data.success == true) {
                    department = data.department
                    if (accion == 'Crear') {
                        tblDepartment.row.add([
                            '<span id="tblDepartment_id'+department.id+'">'+department.id+'</span>',
                            '<span id="tblDepartment_name'+department.id+'" >'+department.name+'</span>',
                            '<button class="btn btn-primary" onClick="editar('+department.id+')" ><span class="glyphicon glyphicon-pencil"></span></button>',
                            '<button class="btn btn-danger" onClick="eliminar('+department.id+')" ><span class="glyphicon glyphicon-trash"></span></button>',
                        ]).draw()
                    }else{
                        $('#tblDepartment_name'+department.id).html(department.name)
                    }
                    $('#frmDepartment_leyenda').html('Nuevo Departamento')
                    $('#frmDepartment_hddId').val('')
                    $('#frmDepartment_txtName').val('')
                    $('#frmDepartment_btnEnviar').val('Crear')
                    $('#frmDepartment_btnEnviar').html('Crear')
                    alertify.success(data.message)
                } else {
                    alertify.error(data.message)
                }
            },
            error: function(errors){
                alertify.error("<span style='color:white;'>Error de comunicación con el servidor</span>")
            }
        })
    })

// función de limpieza del formulario
    $('#frmDepartment_btnCancelar').click(function() {
        $('#frmDepartment_leyenda').html('Nuevo Departamento')
        $('#frmDepartment_hddId').val('')
        $('#frmDepartment_txtName').val('')
        $('#frmDepartment_btnEnviar').val('Crear')
        $('#frmDepartment_btnEnviar').html('Crear')
    })

// Función que transfiere los datos de la fila a modificarse, hacia el formulario
    function editar(id) {
        $('#frmDepartment_leyenda').html('Modificar Departamento')
        $('#frmDepartment_hddId').val(id)
        $('#frmDepartment_txtName').val($('#tblDepartment_name'+id).html())
        $('#frmDepartment_btnEnviar').val('Guardar Cambios')
        $('#frmDepartment_btnEnviar').html('Guardar Cambios')
    }

// Función para eliminar un país
    function eliminar(id) {
        alertify.confirm("<center><span style='color:black; font-weight: bold; font-size: 16px;'>¿Desea eliminar este registro?</span></center>").set('onok', function(closeEvent){
            var url = "{{ URL::route('deleteDepartment', ['id' => ':id']) }}"
            url = url.replace(':id', id)
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data) {
                    if (data.success == true) {
                        tblDepartment.row($('#tblDepartment_id'+id).parents('tr')).remove().draw()
                        alertify.success(data.message)
                    } else {
                        alertify.error(data.message)
                    }
                },
                error: function(errors){
                    alertify.error("<span style='color:white;'>Error de comunicacion con el servidor, por favor contacte al encargado de soporte.</span>")
                }
            })
        }).set('modal', false).setHeader('Alerta')
    }
</script>