<div class="panel-heading"><span id="frmDepartment_leyenda" style="font-weight: bold;" >Nueva Área</span></div>
{!! Form::open(['class' => 'form-horizontal', 'role' => 'form', 'name' => 'frmDepartment', 'id' => 'frmDepartment']) !!}
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<br>
				<fieldset>
					<div class="form-group">
						<div class="col-sm-12">
							{!! Form::text('frmDepartment_hddId',null,['class' => 'form-control', 'id' => 'frmDepartment_hddId']) !!}
							{!! Form::text('frmDepartment_hddCountry',null,['class' => 'form-control', 'id' => 'frmDepartment_hddCountry']) !!}
							<b>Departamento: </b>
							{!! Form::text('frmDepartment_txtName',null,['class' => 'form-control', 'id' => 'frmDepartment_txtName']) !!}
						</div>
					</div>
				</fieldset>
			</div>
		</div>
	</div>
	<div class="panel-footer text-right">
		{!! Form::button('Crear',['class' => 'btn btn-primary', 'id' => 'frmDepartment_btnEnviar', 'value' => 'Crear']) !!}
		{!! Form::button('Cancelar',['class' => 'btn btn-default', 'id' => 'frmDepartment_btnCancelar']) !!}
	</div>
{!! Form::close() !!}