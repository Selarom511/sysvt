<nav class="navbar navbar-default" role="navigation" style="height: 20%;">
    <div class="container-fluid" style="height: 20%;">
        <div class="navbar-header" style="height: 20%;">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="menu"><a href="{{ route('home') }}">Votar</a></li>
                <li class="menu"><a href="{{ route('viewCountry') }}">País</a></li>
                <li class="menu"><a href="{{ route('viewDepartment') }}">Departamento</a></li>
                <li class="menu"><a href="{{ route('viewCommittee') }}">Comité</a></li>
                <li class="menu"><a href="{{ route('viewUser') }}">Usuario</a></li>
            </ul>
        </div>
    </div>
</nav>
