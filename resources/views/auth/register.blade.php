@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Nuevo Registro</b></div>
                <div class="panel-body">
                    {!! Form::open(['url' => route('register'), 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'frmRegister']) !!}

                        <div class="form-group{{ $errors->has('id_document') ? ' has-error' : '' }}">
                            <label for="id_document" class="col-md-4 control-label"># de Documento</label>

                            <div class="col-md-6">
                                {!! Form::text('id_document',old('id_document'),['class' => 'form-control', 'id' => 'id_document','required' => true, 'autofocus' => true]) !!}

                                @if ($errors->has('id_document'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_document') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre</label>

                            <div class="col-md-6">
                                {!! Form::text('name',old('name'),['class' => 'form-control', 'id' => 'name','required' => true]) !!}

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                            <label for="surname" class="col-md-4 control-label">Apellidos</label>

                            <div class="col-md-6">
                                {!! Form::text('surname',old('surname'),['class' => 'form-control', 'id' => 'surname','required' => true]) !!}

                                @if ($errors->has('surname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-6">
                                {!! Form::text('email',old('email'),['class' => 'form-control', 'id' => 'email','required' => true]) !!}

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                {!! Form::password('password',['class' => 'form-control', 'id' => 'password','required' => true]) !!}

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirmar Contraseña</label>

                            <div class="col-md-6">
                                {!! Form::password('password_confirmation',['class' => 'form-control', 'id' => 'password_confirmation','required' => true]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="country" class="col-md-4 control-label">País</label>

                            @php
                                $countries = App\Country::pluck('name','id');
                                $countryArray = [0 => 'Seleccione un país'];
                                foreach ($countries as $key => $value)
                                    $countryArray[$key] = $value;
                            @endphp
                            <div class="col-md-6">
                                {!! Form::select('country',$countryArray,0,['class' => 'form-control', 'id' => 'country','required' => true]) !!}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
                            <label for="department" class="col-md-4 control-label">Departamento</label>

                            <div class="col-md-6">
                                {!! Form::select('department',[0 => 'Seleccione un departamento'],0,['class' => 'form-control', 'id' => 'department','required' => true, 'disabled' => true]) !!}

                                @if ($errors->has('department'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('department') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12">&nbsp;</div>

                        <div class="form-group">
                            <div class="col-md-4 col-md-offset-2">
                                <div class="text-left">
                                    {!! Form::button('Registrar',['class' => 'btn btn-primary btn-block', 'id' => 'btnEnviar', 'value' => 'Registrar']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="text-right">
                                    <a class="btn btn-default btn-block" href="javascript:history.back();" target="_blank" >Cancelar</a>
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    // Función de búsqueda de departamentos en relación al país seleccionado
        $('#country').change(function() {
            if ($('#country').val() == 0) {
                $('#department').html('<option value="0" selected>Seleccione un departamento</option>')
                $('#department').attr('disabled',true)
            } else {
                var url = "{{ URL::route('getDepartments', ['id' => ':id']) }}"
                url = url.replace(':id', ($('#country').val()))
                $.ajax({
                    type: 'GET',
                    url: url,
                    success: function (data) {
                        if (data.success == true) {
                            $('#department').html('')
                            for (var i = 0; i < data.departmentArray.length; i++) {
                                var department = data.departmentArray[i]
                                $('#department').append("<option value='"+department.id+"'>"+department.name+"</option>")
                            }
                            $('#department').attr('disabled',false)
                        } else {
                            $('#department').attr('disabled',true)
                            alertify.error(data.message)
                        }
                    },
                    error: function(errors){
                        alertify.error("<span style='color:white;'>Error de comunicacion con el servidor, por favor contacte al encargado de soporte.</span>")
                    }
                })
            }
        })

    // Evento registrar usuario
        $('#btnEnviar').click(function() {
            if ($('#department').val() != 0)
                $('#frmRegister').submit()
            else
                alertify.alert("<center><span style='color:black; font-weight: bold; font-size: 16px;'>Por favor seleccione un departamento.</span></center>").setHeader('Alerta')
        })
</script>
@endsection
