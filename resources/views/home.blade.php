@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center">
            <div id="page1">
                <a id="btnVotar"  href="#" class="btn btn-sq-lg btn-primary text-center" style="padding-top: 40px;" >
                    <i class="glyphicon glyphicon-pencil" style="font-size: 50px;" ></i><br/>
                    <div style="font-size: 30px;">Votar</div>
                </a>
            </div>
        </div>
        <div class="col-md-12 text-center">&nbsp;</div>
        <div class="col-md-12 text-center">
            <div id="page2">
            </div>
        </div>
        <div class="col-md-12 text-center">&nbsp;</div>
        <div class="col-md-12 text-center">
            <div id="page3">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$('#btnVotar').click(function() {
    var url = "{{ URL::route('getCommittee') }}"
    $.ajax({
        type: 'GET',
        url: url,
        success: function (data) {
            if (data.success == true) {
                if (data.committeeArray.length > 0) {
                    $('#page2').html('').append('<div class="col-md-12 text-center"><b>Comités</b></div>')
                    for (var i = 0; i < data.committeeArray.length; i++) {
                        var committee = data.committeeArray[i]
                        $('#page2').append(
                            '<div class="col-md-3">'+
                                '<button class="btn btn-primary btn-block" onclick="candidate('+committee.id+')">'+committee.name+'</button>'+
                            '</div>')
                    }
                } else {
                    $('#page2').html('').append(
                        '<div class="col-md-12">No hay comites para votar</div>')
                }
            } else {
                alertify.error(data.message)
            }
        },
        error: function(errors){
            alertify.error("<span style='color:white;'>Error de comunicacion con el servidor, por favor contacte al encargado de soporte.</span>")
        }
    })
})

function candidate(id) {
    var url = "{{ URL::route('getCandidate', ['comit' => ':comit']) }}"
    url = url.replace(':comit', id)
    $.ajax({
        type: 'GET',
        url: url,
        success: function (data) {
            if (data.success == true) {
                if (data.candidateArray.length > 0) {
                    $('#page3').html('').append('<div class="col-md-12 text-center"><b>Candidatos</b></div>')
                    for (var i = 0; i < data.candidateArray.length; i++) {
                        var candidate = data.candidateArray[i]
                        var url2 = "{{ URL::route('vote', ['cand' => ':cand']) }}"
                        url2 = url2.replace(':cand', candidate.id)
                        $('#page3').append(
                            '<div class="col-md-3">'+
                                '<a class="btn btn-primary btn-block" href="'+url2+'">'+candidate.name+'</a>'+
                            '</div>')
                    }
                } else {
                    $('#page3').html('').append(
                        '<div class="col-md-12">No hay candidatos para votar</div>')
                }
            } else {
                alertify.error(data.message)
            }
        },
        error: function(errors){
            alertify.error("<span style='color:white;'>Error de comunicacion con el servidor, por favor contacte al encargado de soporte.</span>")
        }
    })
}
</script>

@endsection
