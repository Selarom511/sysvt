<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ config('app.name') }} - @yield('titulo')</title>
    <!-- CSS Propio -->
    <!-- <link rel="stylesheet" href="{{ URL::asset('css/own_style.css') }}"> -->

    <!-- JQuery -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery.js') }}"></script>

    <!-- JQuery-UI -->
    <link rel="stylesheet" href="{{ URL::asset('plugins/jquery-ui/css/jquery-ui.css') }}">
    <script type="text/javascript" src="{{ URL::asset('plugins/jquery-ui/js/jquery-ui.min.js') }}"></script>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap-theme.css') }}">
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

    <!-- Alertify -->
    <link href="{{ URL::asset('plugins/alertify/css/alertify.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('plugins/alertify/css/themes/semantic.css') }}" rel="stylesheet" />
    <script src="{{ URL::asset('plugins/alertify/js/alertify.js') }}"></script>

    <!-- DataTables -->
    <link rel="stylesheet" href="{{ URL::asset('plugins/dataTables/css/jquery.dataTables.css') }}">
    <script type="text/javascript" src="{{ URL::asset('plugins/dataTables/js/jquery.dataTables.js') }}"></script>

    <!-- Select2 -->
    <link href="{{ URL::asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <script src="{{ URL::asset('plugins/select2/js/select2.min.js') }}"></script>

    @yield('style')
</head>
<body>
	@yield('master')
</body>
</html>