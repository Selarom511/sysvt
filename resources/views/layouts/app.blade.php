@extends('layouts.libs')

@section('titulo')
    @yield('subtitulo')
@stop

@section('style')
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <style type="text/css">
    .navbar-nav a, .navbar-nav #master_alias, .navbar-nav #btnSistemas, .navbar-nav li a, .navbar-nav li ul li a { font-weight: bold; }
    table.dataTable tbody td, td { vertical-align: middle; }
    .btn-sq-lg { width: 200px !important; height: 200px !important; }
    </style>
@stop

@section('master')
    <div id="app">
        <nav class="navbar navbar-default " role="navigation" >
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    @if (Auth::check())
                        <ul class="nav navbar-nav nav-pills" >
                            <div class="btn-group">
                                @php
                                    $number = 1;
                                    $content = Storage::get( 'visits.txt' );
                                    $dates = explode("\n", $content);
                                    foreach($dates as $date) {
                                        $visits = explode('|',$date);
                                        if(date('d-m-Y',strtotime($visits[0])) == date('d-m-Y'))
                                            $number = $visits[1];
                                    }
                                @endphp
                                <button type="button" class="btn btn-success navbar-btn"><div id="master_alias">Visitantes <span id="visits_number" class="badge" style="font-size: 14px;" >{{ $number }}</span></div></button>
                            </div>
                        </ul>
                        <ul class="nav navbar-nav navbar-right nav-pills" >
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary navbar-btn"><div id="master_alias">{{ Auth::user()->alias() }}</div></button>
                                <a class="btn btn-danger navbar-btn" id="logout" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" ><span class="glyphicon glyphicon-off"></span></a>
                            </div>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </ul>
                    @endif
                </div>
            </div>
        </nav>
        @if (Auth::check() && Auth::user()->admin)
            @include('menu.menu')
        @endif

        <div class="container">
            @yield('content')
        </div>

        @if (Auth::check())
            <script type="text/javascript">
                $(document).ready(function() {
                    $(this).on('focusin', function(e) {
                        if ($(e.target).closest(".mce-window").length) {
                            e.stopImmediatePropagation();
                        }
                    })

                    var tiempo = 100;
                    window.setInterval(function() {
                        var url = "{{ URL::route('getVisits') }}"
                        $.ajax({
                            type: 'GET',
                            url: url,
                            success: function (data) {
                                if (data.success == true) {
                                    $('#visits_number').html(data.number)
                                }
                            },
                            error: function(errors){
                            }
                        })
                    }, tiempo);
                })
            </script>
        @endif
    </div>
@stop
