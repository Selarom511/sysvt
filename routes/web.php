<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Auth::routes();

//Route::get('/home', 'HomeController@index');

Route::group(['prefix' => 'config'], function(){
	Route::group(['prefix' => 'country'], function() {
		Route::get('view',['as' => 'viewCountry', 'uses' => 'Config\CountryController@view']);
		Route::post('create',['as' => 'createCountry', 'uses' => 'Config\CountryController@create']);
		Route::post('update',['as' => 'updateCountry', 'uses' => 'Config\CountryController@update']);
		Route::get('delete/{id}',['as' => 'deleteCountry', 'uses' => 'Config\CountryController@delete']);
	});

	Route::group(['prefix' => 'department'], function() {
		Route::get('view',['as' => 'viewDepartment', 'uses' => 'Config\DepartmentController@view']);
		Route::get('search/{id}',['as' => 'searchDepartment', 'uses' => 'Config\DepartmentController@search']);
		Route::post('create',['as' => 'createDepartment', 'uses' => 'Config\DepartmentController@create']);
		Route::post('update',['as' => 'updateDepartment', 'uses' => 'Config\DepartmentController@update']);
		Route::get('delete/{id}',['as' => 'deleteDepartment', 'uses' => 'Config\DepartmentController@delete']);
	});

	Route::group(['prefix' => 'committee'], function() {
		Route::get('view',['as' => 'viewCommittee', 'uses' => 'Config\CommitteeController@view']);
		Route::get('search/{id}',['as' => 'searchCommittee', 'uses' => 'Config\CommitteeController@search']);
		Route::post('create',['as' => 'createCommittee', 'uses' => 'Config\CommitteeController@create']);
		Route::post('update',['as' => 'updateCommittee', 'uses' => 'Config\CommitteeController@update']);
		Route::get('delete/{id}',['as' => 'deleteCommittee', 'uses' => 'Config\CommitteeController@delete']);
	});

	Route::group(['prefix' => 'user'], function() {
		Route::get('view',['as' => 'viewUser', 'uses' => 'Config\UserController@view']);
		Route::post('search',['as' => 'searchUser', 'uses' => 'Config\UserController@search']);
		Route::post('committee',['as' => 'committeeUser', 'uses' => 'Config\UserController@setCandidate']);
		Route::get('admin/{id}',['as' => 'adminUser', 'uses' => 'Config\UserController@setAdmin']);
		Route::get('delete/{id}',['as' => 'deleteUser', 'uses' => 'Config\UserController@delete']);
	});
});

Route::get('visits',['as' => 'getVisits', 'uses' => 'HomeController@getVisits']);
Route::get('getDepartments/{id}',['as' => 'getDepartments', 'uses' => 'FunctionController@getDepartments']);
Route::get('getCommittees/{id}',['as' => 'getCommittees', 'uses' => 'FunctionController@getCommittees']);
Route::get('getCommittee',['as' => 'getCommittee', 'uses' => 'FunctionController@getCommittee']);
Route::get('getCandidate/{id}',['as' => 'getCandidate', 'uses' => 'FunctionController@getCandidate']);
Route::get('vote/{id}',['as' => 'vote', 'uses' => 'FunctionController@vote']);
