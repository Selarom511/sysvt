<?php

namespace App\Http\Controllers\Config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Redirect;
use Response;
use Exception;

use App\Country;

class CountryController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function view()
	{
		$countries = Country::all();

		return view('config.country.view',compact('countries'));
	}

	public function create(Request $request)
	{
		try {
			$country = Country::create(['name' => $request['frmCountry_txtName']]);

			return Response::json(array('success' => true, 'message' => '<span style="color:white;">Registro agregado.</span>', 'country' => $country));
		} catch (Exception $e) {
			return Response::json(array('success' => false,'message' => '<span style="color:white;">Hubo un problema al ingresar el registro, por favor intente refrescar la ventana.</span>'));
		}
	}

	public function update(Request $request)
	{
		try {

			$country = Country::find($request['frmCountry_hddId']);
			$country->name = $request['frmCountry_txtName'];
			$country->save();

			return Response::json(array('success' => true, 'message' => '<span style="color:white;">Registro actualizado.</span>', 'country' => $country));
		} catch (Exception $e) {
			return Response::json(array('success' => false,'message' => '<span style="color:white;">Hubo un problema al ingresar el registro, por favor intente refrescar la ventana.</span>'));
		}
	}

	public function delete($id)
	{
		try {
			Country::find($id)->delete();

			return Response::json(array("success" => true, "message" => "<span style='color:white;'>Registro eliminado.</span>"));
		} catch (Exception $e) {
			return Response::json(array("success" => false, "message" => "<span style='color:white;'>Ha ocurrido un error al cargar los registros.</span>"));
		}
	}
}
