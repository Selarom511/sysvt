<?php

namespace App\Http\Controllers\Config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Redirect;
use Response;
use Exception;

use App\Country;
use App\Department;

class DepartmentController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function view()
	{
		$countries = Country::pluck('name','id');
		$countryArray = [0 => 'Seleccione un país'];
		foreach ($countries as $key => $value)
			$countryArray[$key] = $value;

		return view('config.department.view',compact('countryArray'));
	}

	public function search($id)
	{
		try {
			$departments = Department::where('country_id',$id)->get();

			return Response::json(array("success" => true, "message" => "<span style='color:white;'>Datos cargados exitosamente.</span>",'departments' => $departments));
		} catch (Exception $e) {
			return Response::json(array("success" => false, "message" => "<span style='color:white;'>Ha ocurrido un error al cargar los registros.</span>"));
		}
	}

	public function create(Request $request)
	{
		try {
			$department = new Department;
			$department->name = $request['frmDepartment_txtName'];
			$department->country()->associate(Country::find($request['frmDepartment_hddCountry']));
			$department->save();

			return Response::json(array('success' => true, 'message' => '<span style="color:white;">Registro agregado.</span>', 'department' => $department));
		} catch (Exception $e) {
			return Response::json(array('success' => false,'message' => '<span style="color:white;">Hubo un problema al ingresar el registro, por favor intente refrescar la ventana.</span>'));
		}
	}

	public function update(Request $request)
	{
		try {

			$department = Department::find($request['frmDepartment_hddId']);
			$department->name = $request['frmDepartment_txtName'];
			$department->save();

			return Response::json(array('success' => true, 'message' => '<span style="color:white;">Registro actualizado.</span>', 'department' => $department));
		} catch (Exception $e) {
			return Response::json(array('success' => false,'message' => '<span style="color:white;">Hubo un problema al ingresar el registro, por favor intente refrescar la ventana.</span>'));
		}
	}

	public function delete($id)
	{
		try {
			Department::find($id)->delete();

			return Response::json(array("success" => true, "message" => "<span style='color:white;'>Registro eliminado.</span>"));
		} catch (Exception $e) {
			return Response::json(array("success" => false, "message" => "<span style='color:white;'>Ha ocurrido un error al cargar los registros.</span>"));
		}
	}
}
