<?php

namespace App\Http\Controllers\Config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Redirect;
use Response;
use Exception;

use App\Country;
use App\Department;
use App\Committee;

class CommitteeController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function view()
	{
		$countries = Country::pluck('name','id');
		$countryArray = [0 => 'Seleccione un país'];
		foreach ($countries as $key => $value)
			$countryArray[$key] = $value;

		$departmentArray = [0 => 'Seleccione un departamento'];

		return view('config.committee.view',compact('countryArray','departmentArray'));
	}

	public function search($id)
	{
		try {
			$committees = Committee::where('department_id',$id)->get();

			return Response::json(array("success" => true, "message" => "<span style='color:white;'>Datos cargados exitosamente.</span>",'committees' => $committees));
		} catch (Exception $e) {
			return Response::json(array("success" => false, "message" => "<span style='color:white;'>Ha ocurrido un error al cargar los registros.</span>"));
		}
	}

	public function create(Request $request)
	{
		try {
			$committee = new Committee;
			$committee->name = $request['frmCommittee_txtName'];
			$committee->department()->associate(Department::find($request['frmCommittee_hddDepartment']));
			$committee->save();

			return Response::json(array('success' => true, 'message' => '<span style="color:white;">Registro agregado.</span>', 'committee' => $committee));
		} catch (Exception $e) {
			return Response::json(array('success' => false,'message' => '<span style="color:white;">Hubo un problema al ingresar el registro, por favor intente refrescar la ventana.</span>'));
		}
	}

	public function update(Request $request)
	{
		try {

			$committee = Committee::find($request['frmCommittee_hddId']);
			$committee->name = $request['frmCommittee_txtName'];
			$committee->save();

			return Response::json(array('success' => true, 'message' => '<span style="color:white;">Registro actualizado.</span>', 'committee' => $committee));
		} catch (Exception $e) {
			return Response::json(array('success' => false,'message' => '<span style="color:white;">Hubo un problema al ingresar el registro, por favor intente refrescar la ventana.</span>'));
		}
	}

	public function delete($id)
	{
		try {
			Committee::find($id)->delete();

			return Response::json(array("success" => true, "message" => "<span style='color:white;'>Registro eliminado.</span>"));
		} catch (Exception $e) {
			return Response::json(array("success" => false, "message" => "<span style='color:white;'>Ha ocurrido un error al cargar los registros.</span>"));
		}
	}
}
