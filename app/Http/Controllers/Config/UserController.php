<?php

namespace App\Http\Controllers\Config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Hash;
use Redirect;
use Response;
use Exception;
use Bus;

use App\Jobs\Mail\Usuario\NewJob as NewMailJob;

use App\Department;
use App\Country;
use App\Committee;
use App\User;

class UserController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function view()
	{
		$countries = Country::pluck('name','id');
		$countryArray = [0 => 'Seleccione un país'];
		foreach ($countries as $key => $value) {
			$countryArray[$key] = $value;
		}
		$departmentArray = [0 => 'Seleccione un departamento'];
		$committeeArray = Committee::pluck('name','id');

		return view('config.user.view', compact('countryArray','departmentArray','committeeArray'));
	}

	public function search(Request $request)
	{
		try {
			$users = User::where('id','<>',1);
			$users = (($request['frmSearchUser_txtName'] == '') ? $users : $users->where('name','like','%'.$request['frmSearchUser_txtName'].'%'));
			$users = (($request['frmSearchUser_txtSurname'] == '') ? $users : $users->where('surname','like','%'.$request['frmSearchUser_txtSurname'].'%'));
			$users = (($request['frmSearchUser_txtDocument_id'] == '') ? $users : $users->where('id_document','like','%'.$request['frmSearchUser_txtDocument_id'].'%'));
			$users = (($request['frmSearchUser_txtEmail'] == '') ? $users : $users->where('email','like','%'.$request['frmSearchUser_txtEmail'].'%'));
			if ($request['frmSearchUser_slcDepartment'] != 0) 
				$users = $users->where('department_id',$request['frmSearchUser_slcDepartment']);
			else if ($request['frmSearchUser_slcCountry'] != 0 AND $request['frmSearchUser_slcDepartment'] == 0) 
				$users = $users->whereIn('department_id',Department::where('country_id',$request['frmSearchUser_slcCountry'])->pluck('id'));
			$users = $users->get();
			if (sizeof($users) > 0) {
				foreach ($users as $key => $user) {
					$user['department_nom'] = $user->department->name;
					$user['country_nom'] = $user->department->country->name;
					$user['country_id'] = $user->department->country->id;
					$user['admin_nom'] = (($user->admin == true) ? 'Si' : 'No');

					$committees = '';
					foreach ($user->committee()->get() as $key2 => $committee)
						$committees .= ','.$committee->id;
					$user['committees'] = (($committees != '') ? substr($committees, 1) : $committees);
				}
			}
			return Response::json(array("success" => true, "message" => "<span style='color:white;'>Datos cargados exitosamente.</span>",'users' => $users));
		} catch (Exception $e) {
			return Response::json(array("success" => false, "message" => "<span style='color:white;'>Ha ocurrido un error al cargar los registros.</span>"));
		}
	}

	public function setCandidate(Request $request)
	{
		try {
			$user = User::find($request['frmUser_hddId']);
			$user->committee()->detach();
			$user->committee()->attach($request['frmUser_slcActCommittee']);
			$user->committee()->attach($request['frmUser_slcCommittee']);

			$committees_id = '';
			foreach ($user->committee()->get() as $key => $committee) {
				$committees_id .= ','.$committee->id;
			}
			$user['committees'] = (($committees_id != '') ? substr($committees_id, 1) : $committees_id);

			return Response::json(array('success' => true, 'message' => '<span style="color:white;">Registro agregado.</span>', 'user' => $user));
		} catch (Exception $e) {
			return Response::json(array('success' => false,'message' => '<span style="color:white;">Hubo un problema al ingresar el registro, por favor intente refrescar la ventana.</span>'));
		}
	}

	public function setAdmin($id)
	{
		try {
			$user = User::find($id);
			$user->admin = (($user->admin == true) ? false : true);
			$user->save();

			return Response::json(array('success' => true, 'message' => '<span style="color:white;">Registro actualizado.</span>', 'user' => $user));
		} catch (Exception $e) {
			return Response::json(array('success' => false,'message' => '<span style="color:white;">Hubo un problema al ingresar el registro, por favor intente refrescar la ventana.</span>'));
		}
	}

	public function delete($id)
	{
		try {
			User::find($id)->delete();

			return Response::json(array("success" => true, "message" => "<span style='color:white;'>Registro eliminado.</span>"));
		} catch (Exception $e) {
			return Response::json(array("success" => false, "message" => "<span style='color:white;'>Ha ocurrido un error al cargar los registros.</span>"));
		}
	}
}
