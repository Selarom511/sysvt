<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Redirect;
use Response;
use Exception;

use App\Department;
use App\Committee;
use App\User;
use App\Candidate;
use App\Vote;

class FunctionController extends Controller
{
	public function getDepartments($id)
	{
		try {
			$departments = Department::where('country_id',$id)->pluck('name','id');
			$departmentArray = [['id' => 0,'name' => 'Seleccione un departamento']];
			foreach ($departments as $key => $value) {
				$departmentArray[] = ['id' => $key,'name' => $value];
			}
			return Response::json(array("success" => true, "message" => "<span style='color:white;'>Datos cargados exitosamente.</span>",'departmentArray' => $departmentArray));
		} catch (Exception $e) {
			return Response::json(array("success" => false, "message" => "<span style='color:white;'>Ha ocurrido un error al cargar los registros.</span>"));
		}
	}

	public function getCommittees($id)
	{
		try {
			$committee = Committee::where('department_id',$id)->pluck('committee.name','committee.id');
			$committeeArray = [['id' => 0,'name' => 'Seleccione un comité']];
			foreach ($committee as $key => $value) {
				$committeeArray[] = ['id' => $key,'name' => $value];
			}
			return Response::json(array("success" => true, "message" => "<span style='color:white;'>Datos cargados exitosamente.</span>",'committeeArray' => $committeeArray));
		} catch (Exception $e) {
			return Response::json(array("success" => false, "message" => "<span style='color:white;'>Ha ocurrido un error al cargar los registros.</span>"));
		}
	}

	public function getCommittee()
	{
		try {
			$vote = Vote::where('user_id',Auth::user()->id);
			$votes = ((sizeof($vote->get()) > 0) ? $vote->pluck('candidate_id') : []);
			$candidates = Candidate::whereIn('id',$votes);
			$notCommittes = ((sizeof($candidates->get()) > 0) ? $candidates->pluck('committee_id') : []);
			$committee = Committee::where('department_id',Auth::user()->department_id)->whereNotIn('id',$notCommittes);
			$committees = ((sizeof($committee->get()) > 0) ? $committee->pluck('name','id') : []);
			$committeeArray = [];
			foreach ($committees as $key => $value) {
				$committeeArray[] = ['id' => $key,'name' => $value];
			}
			return Response::json(array("success" => true, "message" => "<span style='color:white;'>Datos cargados exitosamente.</span>",'committeeArray' => $committeeArray));
		} catch (Exception $e) {
			return Response::json(array("success" => false, "message" => "<span style='color:white;'>Ha ocurrido un error al cargar los registros.</span>"));
		}
	}

	public function getCandidate($id)
	{
		try {
			$vote = Vote::where('user_id',Auth::user()->id);
			$votes = ((sizeof($vote->get()) > 0) ? $vote->pluck('candidate_id') : []);
			$notCommitte = Candidate::whereIn('id',$votes);
			$notCommittes = ((sizeof($notCommitte->get()) > 0) ? $notCommitte->pluck('user_id') : []);
			$candidate = Candidate::where('committee_id',$id)->whereNotIn('user_id',$notCommittes);
			$candidates = ((sizeof($candidate->get()) > 0) ? $candidate->pluck('user_id','id') : []);
			$candidateArray = [];
			foreach ($candidates as $key => $value) {
				$candidateArray[] = ['id' => $key,'name' => User::find($value)->name.' '.User::find($value)->surname];
			}
			return Response::json(array("success" => true, "message" => "<span style='color:white;'>Datos cargados exitosamente.</span>",'candidateArray' => $candidateArray));
		} catch (Exception $e) {
			return Response::json(array("success" => false, "message" => "<span style='color:white;'>Ha ocurrido un error al cargar los registros.</span>"));
		}
	}

	public function vote($id)
	{
		$vote = Vote::create(['date' => date('Y-m-d'),'candidate_id' => $id,'user_id' => Auth::user()->id]);

		return redirect()->route('home');
	}
}
