<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Storage;
use Auth;
use Redirect;
use Response;
use Exception;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function getVisits()
    {
        try {
            $number = 1;
            $content = Storage::get( 'visits.txt' );
            $dates = explode("\n", $content);
            foreach($dates as $date) {
                $visits = explode('|',$date);
                if(date('d-m-Y',strtotime($visits[0])) == date('d-m-Y'))
                    $number = $visits[1];
            }

            return Response::json(array("success" => true, "message" => "<span style='color:white;'>Exito.</span>",'number' => $number));
        } catch (Exception $e) {
            return Response::json(array("success" => false, "message" => "<span style='color:white;'>Ha ocurrido un error al cargar los registros.</span>"));
        }
    }
}
