<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use SoftDeletes;

    protected $table = 'department';

    protected $fillable = ['name','country_id'];

    protected $dates = ['deleted_at'];

    /**
     * Relaciones
     */
    public function country(){
        return $this->belongsTo('App\Country');
    }

    public function user(){
        return $this->hasMany('App\User');
    }
    public function committee(){
        return $this->hasMany('App\Committee');
    }
}
