<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes;

    protected $table = 'country';

    protected $fillable = ['name'];

    protected $dates = ['deleted_at'];

    /**
     * Relaciones
     */
    public function department(){
        return $this->hasMany('App\Department');
    }
}
