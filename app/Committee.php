<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Committee extends Model
{
    use SoftDeletes;

    protected $table = 'committee';

    protected $fillable = ['name','department_id'];

    protected $dates = ['deleted_at'];

    /**
     * Relaciones
     */
    public function department(){
        return $this->belongsTo('App\Department');
    }

    public function candidate(){
        return $this->hasMany('App\Candidate');
    }

    public function user(){
        return $this->belongsToMany('App\User','candidate');
    }
}
