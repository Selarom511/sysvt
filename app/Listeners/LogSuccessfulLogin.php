<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Storage;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $dato = 1;
        if (Storage::exists('visits.txt')) {
            $content = Storage::get( 'visits.txt' );
            $visitsArray = explode("\n", $content);
            $flag = false;
            foreach($visitsArray as $visit) {
                $number = explode('|',$visit);
                if (date('d-m-Y',strtotime($number[0])) == date('d-m-Y')) {
                    $flag = true;
                    $search = '/'.$number[0].'\|'.$number[1].'/';
                    $number[1] += 1;
                    $replace = $number[0].'|'.$number[1];
                    $content = preg_replace($search, $replace, $content);
                    $dato = $number[1];
                }
            }
            if (!$flag) {
                $content = date('d-m-Y').'|'.$dato;
                Storage::append( 'visits.txt', $content );
            } else {
                Storage::put( 'visits.txt', $content );
            }
        } else {
            $content = date('d-m-Y').'|'.$dato;
            Storage::put( 'visits.txt', $content );
        }
    }
}
