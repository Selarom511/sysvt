<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vote extends Model
{
    use SoftDeletes;

    protected $table = 'vote';

    protected $fillable = ['date','user_id','candidate_id'];

    protected $dates = ['date','deleted_at'];

    /**
     * Relaciones
     */
    public function candidate(){
        return $this->belongsTo('App\Candidate');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
}
