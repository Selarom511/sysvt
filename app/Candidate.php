<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Candidate extends Model
{
    use SoftDeletes;

    protected $table = 'candidate';

    protected $fillable = ['committee_id','user_id'];

    protected $dates = ['deleted_at'];

    /**
     * Relaciones
     */
    public function committee(){
        return $this->belongsTo('App\Committee');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function vote(){
        return $this->hasMany('App\Vote');
    }

    public function users(){
        return $this->belongsToMany('App\User','vote');
    }
}
