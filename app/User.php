<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_document','name','surname','email', 'password','admin','department_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $dates = ['deleted_at'];

    /**
     * Relaciones
     */
    public function department(){
        return $this->belongsTo('App\Department');
    }

    public function candidate(){
        return $this->hasMany('App\Candidate');
    }
    public function vote(){
        return $this->hasMany('App\Vote');
    }

    public function committee(){
        return $this->belongsToMany('App\Committee','candidate');
    }
    public function candidates(){
        return $this->belongsToMany('App\Candidate','vote');
    }

    /**
     * Funciones
     */
    public function alias(){
        return $this->name.' '.$this->surname;
    }
}
